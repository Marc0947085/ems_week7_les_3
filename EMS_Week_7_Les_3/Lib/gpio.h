#ifndef _GPIO_H
#define _GPIO_H
/**
 *
 * \defgroup GPIO
 * @brief Alles wat is gerelateerd aan de input/output pinnen
 *
 * De pinnen op de microcontroller moeten worden ingesteld. Stel jezelf de
 * volgende vragen op het moment dat je pinnen moet gebruiken:
 *
 * + Worden de pinnen gebruikt voor een speciale functies (bijv. I2C) @see gpioSetFunction
 * + Worden de pinnen gebruikt voor input of output? @see gpioSetDirection
 * + Moet een digitale pin een logische 1 of 0 uitsturen? @see gpioSet
 *
 * @{
 *
 */


/*!
 * @brief Macro om bitje 0 te maken binnen een register.
 *
 * Deze regel beschrijft een zogenaamde macro. Deze specifieke macro kun je zien
 * als een slim 'search and replace' commando voor de preprocessor (deel van het build proces).
 * Overal waar 'clearBit(reg,bit)' staat zal dit worden vervangen met 'reg |= (1<<bit)'. Een
 * macro wordt nu toegepast omdat een functie pointers nodig zou hebben om hetzelfde te bereiken.
 */
#define clearBit(reg,bit) 	(reg &= ~(1<<bit))

//! @see clearBit()
#define setBit(reg,bit) 	(reg |= (1<<bit))

//! @see clearBit()
#define setBits(reg,bits) 	(reg |= (bits))
//! @see clearBit()
#define clearBits(reg,bits) 	(reg &= ~(bits))

//!Pin richting
typedef enum {INPUT, OUTPUT} gpioDirection;
//!Pin waarde
typedef enum {LOW, HIGH} gpioValue;
//!Alternatieve pin functies
typedef enum {DIGITAL, PRIMARY, SECONDARY, CAPACITIVESENSING} gpioFunction;

/*!
 * @brief Elke pin kan een ingang of uitgangspin worden. Met deze functie kun je het instellen.
 *
 * @code
 * //Stel P1.5 in als een output pin
 * gpioSetDirection(1,5, OUTPUT);
 * @endcode
 *
 * \param port De port van de pin (1 of 2)
 * \param pin De in te stellen pin (0 t/m 7)
 * \param dir Kies OUTPUT (1) of INPUT (0)
 */
void gpioSetDirection(uint8_t port, uint8_t pin, gpioDirection dir);


/*!
 * @brief Stel de waarde in van de uitgangspin.
 *
 * Stel met deze functie de logische waarde in van een uitgangspin. Let op:
 * Kan een andere uitwerking hebben als de pin in kwestie niet is ingesteld als uitgang.
 * \param port De port van de pin (1 of 2)
 * \param pin De in te stellen pin (0 t/m 7)
 * \param value Kies HIGH (1) of LOW (0)*
 * @todo De functie houdt er momenteel geen rekening mee dat een pin ook als input ingesteld zou kunnen zijn.
 */
void gpioSet(uint8_t port, uint8_t pin, gpioValue value);


/*!
 * @brief Stel in aan welke functionaliteit de betreffende pin is gekoppeld.
 *
De pinnen van de microcontroller zijn gekoppeld aan verschillende schakelingen en dus functies.

@code
//stel P2.0 in voor de eerste alternatieve functie
gpioSetFunction(2,0,PRIMARY);
//Stel P1.7 in voor de tweede alternatieve functie
gpioSetFunction(1,7,SECONDARY);
//Stel P1.7 weer in als een digitale I/O pin
gpioSetFunction(1,7,DIGITAL);
@endcode

Deze tabel beschrijf alle primary en secundaire functies die zijn gekoppeld aan de pinnen.
Let op: wanneer de pin wordt gebruikt voor ADC10, Capactieve Detectie of als JTAG, dan
heeft deze functie geen invloed.

Pin  |Primary  	|Secondary        |
-----|----------|-----------------|
P1.0 |TACLK		|                 |
P1.1 |TA0.0		|UCA0RXD          |
P1.2 |TA0.1		|UCA0TXD          |
P1.3 |ADC10CLK	|CAOUT            |
P1.4 |SMCLK		|UCB0STE/UCA0CLK  |
P1.5 |TA0.0		|UCB0CLK/UCA0STE  |
P1.6 |TA0.1		|UCB0SCL/UCB0SOMI |
P1.7 |CAOUT		|UCB0SDA/UCB0SIMO |
P2.X |TIMER1_A3	|                 |

\param port De port van de pin (1 of 2)
\param pin De in te stellen pin (0 t/m 7)
\param function Mogelijk te selecteren functies zijn DIGITAL (standaard), PRIMARY, SECONDARY, CAPACITIVESENSING.

 */
void gpioSetFunction(uint8_t port, uint8_t pin, gpioFunction function);

/**
 * @}
 */
#endif
